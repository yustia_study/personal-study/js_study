const express = require('express');
const fs = require('fs');
const router = express.Router();
const path = require('path');

const template = require('../lib/template.js')
  
router.get('/create', (req, res) => {
    let title = 'Web - create';
    let list = template.list(req.list);
    let html = template.html(title, list, `
      <form action='/page/process_create' method='post'>
      <p><input type='text' name='title' placeholder='title'></p>
      <p>
        <textarea name='description' placeholder='description'></textarea>
      </p>
      <p>
        <input type='submit'>
      </p>
      </form>`, ''  
    );
      res.send(html);
});
  
router.post('/process_create', (req, res) => {
    let title = new URLSearchParams(req.body).get('title');
    let description = new URLSearchParams(req.body).get('description');
    fs.writeFile(`data/${title}`, description, 'utf8', function(err){
      res.redirect(`/?id=${title}`);
    });
});
  
router.get('/update/:pageId', (req, res) => {
    let filteredId = path.parse(req.params.pageId).base;
    fs.readFile(`data/${filteredId}`, 'utf8', function(err, description) {
      let title = req.params.pageId;
      let list = template.list(req.list);
      let html = template.html(title, list,
        `<form action='/page/process_update' method='post'>
        <input type='hidden' name='id' value='${title}'>
        <p><input type='text' name='title' placeholder='title' value='${title}'></p>
        <p>
            <textarea name='description' placeholder='description'>${description}</textarea>
        </p>
        <p>
            <input type='submit'>
        </p>
        </form>`,
        `<a href='/page/create'>create</a> <a href='/page/update/${title}'>update</a>`
        );
      res.send(html);
    });
});
  
router.post('/process_update', (req, res) => {
    let id = new URLSearchParams(req.body).get('id');
    let title = new URLSearchParams(req.body).get('title');
    let description = new URLSearchParams(req.body).get('description');
    let filteredId = path.parse(id).base;
    fs.rename(`data/${filteredId}`, `data/${title}`, function(err){
      fs.writeFile(`data/${title}`, description, 'utf8', function(err){
        res.redirect(`/?id=${title}`);
      });
    });
});
  
router.post('/process_delete', (req, res) => {
    let id = new URLSearchParams(req.body).get('id');
    fs.unlink(`data/${id}`, function(err){
      res.redirect('/');
    });
});

module.exports = router;