const express = require('express');
const compression = require('compression');
const fs = require('fs');
const bodyParser = require('body-parser');
const indexRouter = require('./routes/index.js');
const pageRouter = require('./routes/page.js');
const sanitizeHtml = require('sanitize-html');
const app = express();
const template = require('./lib/template.js')
const path = require('path');
const port = 3000;
;
app.use(bodyParser.urlencoded({extended: false}));
app.use(compression());
app.get('*', function(req, res, next){
  fs.readdir('./data', function(err, filelist){
    req.list = filelist;
    next();
  });
});

app.use('/', indexRouter);
app.use('/page', pageRouter);

app.get('/page/:pageId', (req, res, next) => {
  let filteredId = path.parse(req.params.pageId).base;
  fs.readFile(`data/${filteredId}`, 'utf8', function(err, description) {
    if(err){
      next(err);
    } else {
      let title = req.params.pageId;
      let sanitizedTitle = sanitizeHtml(title);
      let sanitizedDescription = sanitizeHtml(description);
      let list = template.list(req.list);
      let html = template.html(sanitizedTitle, list,
        `<h2>${sanitizedTitle}</h2>${sanitizedDescription}`,
        `<a href='/page/create'>create</a>
        <a href='/page/update/${sanitizedTitle}'>update</a> 
        <form action='/page/process_delete' method='post'>
        <input type='hidden' name='id' value='${sanitizedTitle}'>
        <input type='submit' value='delete'>
        </form>`
      );
      res.send(html);
    }
  });
});

app.use(function(req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});